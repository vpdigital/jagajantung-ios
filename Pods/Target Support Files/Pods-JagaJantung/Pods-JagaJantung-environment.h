
// To check if a library is compiled with CocoaPods you
// can use the `COCOAPODS` macro definition which is
// defined in the xcconfigs so it is available in
// headers also when they are imported in the client
// project.


// REFrostedViewController
#define COCOAPODS_POD_AVAILABLE_REFrostedViewController
#define COCOAPODS_VERSION_MAJOR_REFrostedViewController 2
#define COCOAPODS_VERSION_MINOR_REFrostedViewController 4
#define COCOAPODS_VERSION_PATCH_REFrostedViewController 7

// SHLineGraphView
#define COCOAPODS_POD_AVAILABLE_SHLineGraphView
#define COCOAPODS_VERSION_MAJOR_SHLineGraphView 1
#define COCOAPODS_VERSION_MINOR_SHLineGraphView 0
#define COCOAPODS_VERSION_PATCH_SHLineGraphView 6

// SWTableViewCell
#define COCOAPODS_POD_AVAILABLE_SWTableViewCell
#define COCOAPODS_VERSION_MAJOR_SWTableViewCell 0
#define COCOAPODS_VERSION_MINOR_SWTableViewCell 3
#define COCOAPODS_VERSION_PATCH_SWTableViewCell 7

