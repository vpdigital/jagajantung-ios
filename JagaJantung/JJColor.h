//
//  JJColor.h
//  JagaJantung
//
//  Created by David Christian on 1/6/15.
//  Copyright (c) 2015 CodeSpade. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface JJColor : NSObject
+(UIColor*)themeColor;
+(UIColor*)shadow;
+(UIColor*)progressColor;
@end
