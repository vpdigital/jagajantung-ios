//
//  ItemPenyakit.m
//  JagaJantung
//
//  Created by David Christian on 1/6/15.
//  Copyright (c) 2015 CodeSpade. All rights reserved.
//

#import "ItemPenyakit.h"
#import "JJColor.h"

@implementation ItemPenyakit

-(id)initWithCoder:(NSCoder *)aDecoder{
    self = [super initWithCoder:aDecoder];
    
    self.backgroundColor = [UIColor clearColor];
    [self.titleLabel setTextColor:[UIColor whiteColor]];
    self.layer.cornerRadius = self.frame.size.height / 2;
    self.clipsToBounds = YES;
    self.layer.borderColor = [UIColor whiteColor].CGColor;
    self.layer.borderWidth = 0.5;
    self.adjustsImageWhenHighlighted = NO;
    self.tintColor = [UIColor clearColor];
    
    [self setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [self setTitleColor:[JJColor themeColor] forState:UIControlStateSelected];
    
    [self addTarget:self action:@selector(pressed) forControlEvents:UIControlEventTouchUpInside];
    
    return self;
}

-(void) pressed{
    [self setHighlighted:NO];
    bool selected = self.selected;
    if(selected){
        self.selected = NO;
        self.backgroundColor = [UIColor clearColor];
    }else{
        self.selected = YES;
        self.backgroundColor = [UIColor whiteColor];
    }
}


/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
