//
//  TipsViewController.h
//  JagaJantung
//
//  Created by David Christian on 2/7/15.
//  Copyright (c) 2015 CodeSpade. All rights reserved.
//

#import "BaseWithNavigationViewController.h"
#import <MessageUI/MessageUI.h>

@interface TipsViewController : BaseWithNavigationViewController<UITableViewDataSource, UITableViewDelegate, UIAlertViewDelegate, MFMessageComposeViewControllerDelegate, MFMailComposeViewControllerDelegate>
@property (weak, nonatomic) IBOutlet UITableView *tableView;

@end
