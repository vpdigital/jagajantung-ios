//
//  BloodPressureViewController.h
//  JagaJantung
//
//  Created by David Christian on 1/10/15.
//  Copyright (c) 2015 CodeSpade. All rights reserved.
//

#import "BaseWithNavigationViewController.h"
#import "BloodPressurePicker.h"

@interface BloodPressureViewController : BaseWithNavigationViewController<UIPickerViewDelegate, UIPickerViewDataSource>
@property (weak, nonatomic) IBOutlet BloodPressurePicker *bloodPressurePicker;
@property (weak, nonatomic) IBOutlet UIView *selectionView;
@property (weak, nonatomic) IBOutlet UILabel *dia;
@property (weak, nonatomic) IBOutlet UILabel *sys;

@property NSString *status;
@property NSInteger bpmResult;
@end
