//
//  HeartRateViewController.m
//  JagaJantung
//
//  Created by David Christian on 1/7/15.
//  Copyright (c) 2015 CodeSpade. All rights reserved.
//

#import "HeartRateViewController.h"
#import "HeartRateResultViewController.h"
#import "DejalActivityView.h"
#import "TermsAndConditionsViewController.h"
#import <AVFoundation/AVFoundation.h>

@interface HeartRateViewController ()
@property (strong, nonatomic) YMPulseRateMater *pulseRateMeter;
@end

@implementation HeartRateViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.pulseRateMeter = [YMPulseRateMater new];
    [self.pulseRateMeter setDelegate:self];
    
    [_mulaiBTN setBackgroundColor:[UIColor clearColor]];
    
//    UIImage *normalImage = [UIImage imageNamed:@"circle_button"];
//    UIImage *highlightedImage = [self image:[UIImage imageNamed:@"circle_button"] withAlpha:0.8];
//    
//    [_mulaiBTN setBackgroundColor:[UIColor clearColor]];
//    [_mulaiBTN setBackgroundImage:normalImage forState:UIControlStateNormal];
//    [_mulaiBTN setBackgroundImage:highlightedImage forState:UIControlStateHighlighted];
//    [_mulaiBTN setTintColor:[UIColor grayColor]];
//    _mulaiBTN.adjustsImageWhenHighlighted = NO;
//    _mulaiBTN.layer.cornerRadius = _mulaiBTN.layer.frame.size.height / 2;
}
- (IBAction)toResult:(id)sender {
    
    // check if flashlight available
    Class captureDeviceClass = NSClassFromString(@"AVCaptureDevice");
    if (captureDeviceClass != nil) {
        AVCaptureDevice *device = [AVCaptureDevice defaultDeviceWithMediaType:AVMediaTypeVideo];
        if ([device hasTorch] && [device hasFlash]){
            
            HeartRateResultViewController *destination = [self.storyboard instantiateViewControllerWithIdentifier:@"HeartRateResult"];
            [self.navigationController pushViewController:destination animated:NO];
            
        }else{
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Aplikasi tidak dapat berjalan pada perangkat ini. Perangkat harus memiliki lampu kamera." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            [alert show];
        }
    }

//        [_mulaiBTN setEnabled:NO];
//        [_mulaiBTN setTitle:@"Calculate" forState:UIControlStateDisabled];
//        [self.pulseRateMeter start];
    
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (UIImage *)image:(UIImage*)image withAlpha:(CGFloat) alpha{
    UIGraphicsBeginImageContextWithOptions(image.size, NO, 0.0f);
    
    CGContextRef ctx = UIGraphicsGetCurrentContext();
    CGRect area = CGRectMake(0, 0, image.size.width, image.size.height);
    
    CGContextScaleCTM(ctx, 1, -1);
    CGContextTranslateCTM(ctx, 0, -area.size.height);
    
    CGContextSetBlendMode(ctx, kCGBlendModeMultiply);
    
    CGContextSetAlpha(ctx, alpha);
    
    CGContextDrawImage(ctx, area, image.CGImage);
    
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    
    UIGraphicsEndImageContext();
    
    return newImage;
}

//#pragma PulseRateMeter
//- (void)pulseRateMeter:(id)sender completeWithPulseRate:(float)pulseRate{
////    [DejalBezelActivityView removeView];
//    
//    HeartRateResultViewController *heartRateResultVC = [self.storyboard instantiateViewControllerWithIdentifier:@"HeartRateResult"];
//    heartRateResultVC.bpmResult = pulseRate;
//    [self.navigationController pushViewController:heartRateResultVC animated:NO];
//}
//
//-(void)updateProgress:(float)progress{
//    NSLog([NSString stringWithFormat:@"%f", progress]);
////    [[NSOperationQueue mainQueue] addOperationWithBlock:^ {
//    
//        //Your code goes in here
//        [_progressView setProgress:progress animated:NO];
//        
////    }];
//}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
