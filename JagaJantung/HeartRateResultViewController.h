//
//  HeartRateResultViewController.h
//  JagaJantung
//
//  Created by David Christian on 1/7/15.
//  Copyright (c) 2015 CodeSpade. All rights reserved.
//

#import "BaseWithNavigationViewController.h"
#import "DACircularProgressView.h"
#import "YMPulseRateMater.h"

@interface HeartRateResultViewController : BaseWithNavigationViewController<YMPulseRateMeterDelegate>


@property (weak, nonatomic) IBOutlet DACircularProgressView *progressView;
@property (weak, nonatomic) IBOutlet UILabel *resultLabel;
@property (weak, nonatomic) IBOutlet UILabel *mengukurLabel;

@property NSInteger bpmResult;

@end
