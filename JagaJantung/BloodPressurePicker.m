//
//  BloodPressurePicker.m
//  JagaJantung
//
//  Created by David Christian on 1/10/15.
//  Copyright (c) 2015 CodeSpade. All rights reserved.
//

#import "BloodPressurePicker.h"

@implementation BloodPressurePicker

-(id)initWithCoder:(NSCoder *)aDecoder{
    self =[super initWithCoder:aDecoder];
    
    [self setBackgroundColor:[UIColor clearColor]];
    
    UIImage *selectorImage = [UIImage imageNamed:@"selectorImage.png"]; // You have to make it strechable, probably
    UIView *customSelector = [[UIImageView alloc] initWithImage:selectorImage];
    customSelector.frame = CGRectZero; // Whatever rect to match the UIImagePicker
    [self addSubview:customSelector];
    
    return self;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
