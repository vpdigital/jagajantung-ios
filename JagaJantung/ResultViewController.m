//
//  ResultViewController.m
//  JagaJantung
//
//  Created by David Christian on 1/10/15.
//  Copyright (c) 2015 CodeSpade. All rights reserved.
//

#import "ResultViewController.h"
#import "HeartRateViewController.h"
#import "History.h"
#import "JJColor.h"
#import "BloodPressureViewController.h"
#import "SeeMorePageViewController.h"
#import "HeartRateResultViewController.h"

@interface ResultViewController ()

@end

@implementation ResultViewController{
    NSDate *createDate;
    NSString *status;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    _normalControl.layer.cornerRadius = 5;
    _normalControl.layer.borderColor = [UIColor grayColor].CGColor;
    _normalControl.layer.borderWidth = 1;
    [_normalControl setBackgroundColor:[UIColor lightGrayColor]];
    [_normalControl addTarget:self action:@selector(selectNormal) forControlEvents:UIControlEventTouchUpInside];
    
    _banyakPikiranControl.layer.cornerRadius = 5;
    _banyakPikiranControl.layer.borderColor = [UIColor grayColor].CGColor;
    _banyakPikiranControl.layer.borderWidth = 1;
    [_banyakPikiranControl setBackgroundColor:[UIColor lightGrayColor]];
    [_banyakPikiranControl addTarget:self action:@selector(selectBanyakPikiran) forControlEvents:UIControlEventTouchUpInside];
    
    _santaiControl.layer.cornerRadius = 5;
    _santaiControl.layer.borderColor = [UIColor grayColor].CGColor;
    _santaiControl.layer.borderWidth = 1;
    [_santaiControl setBackgroundColor:[UIColor lightGrayColor]];
    [_santaiControl addTarget:self action:@selector(selectSantai) forControlEvents:UIControlEventTouchUpInside];
    
    _descriptionView.hidden = YES;
    _more.hidden = YES;
    _simpan.hidden = YES;
    
    [_resultLabel setText:[NSString stringWithFormat:@"%d kali per menit", _bpmResult]];

}

-(void)selectNormal{
    [self highlight:_normalControl withLabel:_normalLabel];
    
    [self unhighlight:_banyakPikiranControl withLabel:_banyakPikiranLabel];
    [self unhighlight:_santaiControl withLabel:_santaiLabel];
    
    _descriptionView.hidden = NO;
    _more.hidden = NO;
    _simpan.hidden = NO;
    
    status = @"Normal";
}

-(void)selectBanyakPikiran{
    [self highlight:_banyakPikiranControl withLabel:_banyakPikiranLabel];
    
    [self unhighlight:_normalControl withLabel:_normalLabel];
    [self unhighlight:_santaiControl withLabel:_santaiLabel];
    
    _descriptionView.hidden = NO;
    _more.hidden = NO;
    _simpan.hidden = NO;
    
    status = @"Banyak Pikiran";
    
}

-(void)selectSantai{
    [self highlight:_santaiControl withLabel:_santaiLabel];
    
    [self unhighlight:_banyakPikiranControl withLabel:_banyakPikiranLabel];
    [self unhighlight:_normalControl withLabel:_normalLabel];
    
    _descriptionView.hidden = NO;
    _more.hidden = NO;
    _simpan.hidden = NO;
    
    status = @"Santai";
    
}

-(void)highlight:(UIControl*)control withLabel:(UILabel*)label{
    [control setBackgroundColor:[JJColor themeColor]];
    control.layer.borderColor = [UIColor clearColor].CGColor;
    [label setTextColor:[UIColor whiteColor]];
}

-(void)unhighlight:(UIControl*)control withLabel:(UILabel*)label{
    [control setBackgroundColor:[UIColor lightGrayColor]];
    control.layer.borderColor = [UIColor grayColor].CGColor;
    [label setTextColor:[UIColor blackColor]];
}
- (IBAction)toSimpan:(id)sender {
    BloodPressureViewController *destination = [self.storyboard instantiateViewControllerWithIdentifier:@"BloodPressure"];
    destination.status = status;
    destination.bpmResult = _bpmResult;
    [self.navigationController pushViewController:destination animated:YES];
}
- (IBAction)toLebihLanjut:(id)sender {
    SeeMorePageViewController *destination = [self.storyboard instantiateViewControllerWithIdentifier:@"SeeMore"];
    [self.navigationController pushViewController:destination animated:YES];
}
- (IBAction)toKembali:(id)sender {
    HeartRateResultViewController *heartRate = [self.storyboard instantiateViewControllerWithIdentifier:@"HeartRate"];
    [self.navigationController pushViewController:heartRate animated:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
