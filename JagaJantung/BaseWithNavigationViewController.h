//
//  BaseWithNavigationViewController.h
//  JagaJantung
//
//  Created by David Christian on 1/9/15.
//  Copyright (c) 2015 CodeSpade. All rights reserved.
//

#import "BaseViewController.h"

@interface BaseWithNavigationViewController : BaseViewController
@property BOOL hideRightButton;
@property (strong, nonatomic)UIButton *tncButton;

-(void) showRButton;
-(void) hideRButton;
@end
