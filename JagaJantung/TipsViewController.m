//
//  TipsViewController.m
//  JagaJantung
//
//  Created by David Christian on 2/7/15.
//  Copyright (c) 2015 CodeSpade. All rights reserved.
//

#import "TipsViewController.h"
#import "TipsCell.h"

@interface TipsViewController ()

@end

@implementation TipsViewController{
    NSArray *titles;
    NSArray *descs;
    NSInteger shareIndex;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    titles = [NSArray arrayWithObjects:@"Jantung yang sehat",@"Kebiasaan makan yang sehat", @"Kontrol kolesterol dan tekanan darah", @"Jagalah berat badan", @"Hindari Stres", @"Olahraga", @"Cukup tidur yang berkualitas", @"Jangan merokok atau menggunakan tembakau", @"Disclaimer", nil];
    descs = [NSArray arrayWithObjects:@"Masalah pada jantung dapat dihindari dengan menjaga gaya hidup yang sehat", @"Makanlah yang sehat termasuk yang rendah garam, rendah gula, rendah lemak. Contohnya buah buahan, sayur dan makanan berserat lainnnya. Makan beberapa porsi seminggu yang mengandung ikan seperti salmon dan makarel dapat menurunkan risiko serangan jantung. Minuman beralkohol sebaiknya dihindari", @"Hipertensi adalah faktor risiko yang tinggi. Penyakit ini dikenal sebagi pembunuh diam diam yang dapat menyebabkan serangan jantung. Tekanan darah yang optimal adalah 120/80 mmHg. Kolesterol darah dan gula darah harus diperiksa secara teratur dan diterapi jika kadarnya tinggi", @"Menjaga berat badan sangatlah penting. Untuk mencari tahu berat badan yang tepat, Anda harus menghitung Body Mass Index (BMI). BMI dihitung dengan membagi berat badan (kg) dengan hasil kuadrat tinggi (m). BMI cukup baik namun bukan pedoman yang sempurna. Mengukur lingkar pinggang juga berguna untuk mengukur lemak pada perut Anda", @"Stres diketahui akan merubah kondisi tubuh. Jika sedang stres, otot kita cenderung tegang dan denyut jantung meningkat, demikian pula tekanan darah dan tubuh membutuhkan lebih banyak oksigen sehingga jantung harus bekerja keras", @"Olahraga ringan 30-60 menit akan menurunkan risiko penyakit jantung fatal. Mulai perlahan dan secara bertingkat ditingkatkan intensitas, durasi dan frekuensinya", @"Orang dewasa membutuhkan 7-9 jam tidur perhari. Orang yang tidak cukup tidur akan meningkat tekanan darah dan risiko diabetes, depresi dan obestitas", @"Zat kimmia dalam tembakau dapat merusak jantung dan pembuluh darah, menyebabkan penyempitan pembuluh darah arteri (aterosclerosis). Tidak ada jumlah rokok yang aman dan semakin banyak rokok semakin besar risiko Anda", @"Aplikasi ini tidak menggantikan pemeriksaan fisik oleh dokter dan masih ada kemungkinan ketidak akurasian dalam perhitungan denyut nadi", nil];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 5;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    return [self basicCellAtIndexPath:indexPath];
}

- (TipsCell *)basicCellAtIndexPath:(NSIndexPath *)indexPath {
    TipsCell *cell = [_tableView dequeueReusableCellWithIdentifier:@"TipsCell" forIndexPath:indexPath];
    [self configureBasicCell:cell atIndexPath:indexPath];
    return cell;
}

- (void)configureBasicCell:(TipsCell *)cell atIndexPath:(NSIndexPath *)indexPath {
    [cell.title setText:[titles objectAtIndex:indexPath.row]];
    [cell.desc setText:[descs objectAtIndex:indexPath.row]];
    [cell.button setTag:indexPath.row];
    [cell.button addTarget:self action:@selector(doShare:) forControlEvents:UIControlEventTouchUpInside];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return [self heightForBasicCellAtIndexPath:indexPath];
}

- (CGFloat)heightForBasicCellAtIndexPath:(NSIndexPath *)indexPath {
    static TipsCell *sizingCell = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sizingCell = [self.tableView dequeueReusableCellWithIdentifier:@"TipsCell"];
    });
    
    [self configureBasicCell:sizingCell atIndexPath:indexPath];
    return [self calculateHeightForConfiguredSizingCell:sizingCell];
}

- (CGFloat)calculateHeightForConfiguredSizingCell:(UITableViewCell *)sizingCell {
    [sizingCell setNeedsLayout];
    [sizingCell layoutIfNeeded];
    
    CGSize size = [sizingCell.contentView systemLayoutSizeFittingSize:UILayoutFittingCompressedSize];
    return size.height + 1.0f; // Add 1.0f for the cell separator height
}

-(void) doShare:(UIButton*)sender{
    UIAlertView *shareAlert = [[UIAlertView alloc] initWithTitle:@"Share via" message:@"" delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"Email", @"SMS", nil];
    shareIndex = sender.tag;
    [shareAlert show];
}

#pragma mark - UIAlertView
-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    if(buttonIndex==1){ //via email
        MFMailComposeViewController *mc = [[MFMailComposeViewController alloc] init];
        if([MFMailComposeViewController canSendMail]){
            mc.mailComposeDelegate = self;
            [mc setSubject:@"Jaga Jantung Tips"];
            [mc setMessageBody:[NSString stringWithFormat:@"%@ - %@", [titles objectAtIndex:shareIndex], [descs objectAtIndex:shareIndex]] isHTML:NO];
            [self presentViewController:mc animated:YES completion:NULL];
        }else{
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Can not open mail" message:@"Please check your mail settings" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [alert show];
        }
    }else if(buttonIndex == 2){ //via sms
        MFMessageComposeViewController *controller = [[MFMessageComposeViewController alloc] init];
        if([MFMessageComposeViewController canSendText])
        {
            controller.body = [NSString stringWithFormat:@"%@ - %@", [titles objectAtIndex:shareIndex], [descs objectAtIndex:shareIndex]];
            controller.recipients = [NSArray arrayWithObjects:nil];
            controller.messageComposeDelegate = self;
            [self presentViewController:controller animated:YES completion:nil];
        }
    }
}

#pragma mark - SMS
- (void)messageComposeViewController:(MFMessageComposeViewController *)controller didFinishWithResult:(MessageComposeResult)result
{
    switch (result) {
        case MessageComposeResultCancelled:{
            NSLog(@"Cancelled");
        }
            break;
        case MessageComposeResultFailed:{
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Unknown Error"
                                                           delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
            [alert show];
        }
            break;
        case MessageComposeResultSent:
            
            break;
        default:
            break;
    }
    
    [self dismissViewControllerAnimated:YES completion:nil];
}
#pragma mark - Email
- (void) mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error
{
    switch (result)
    {
        case MFMailComposeResultCancelled:
            NSLog(@"Mail cancelled");
            break;
        case MFMailComposeResultSaved:
            NSLog(@"Mail saved");
            break;
        case MFMailComposeResultSent:
            NSLog(@"Mail sent");
            break;
        case MFMailComposeResultFailed:
            NSLog(@"Mail sent failure: %@", [error localizedDescription]);
            break;
        default:
            break;
    }
    
    // Close the Mail Interface
    [self dismissViewControllerAnimated:YES completion:NULL];
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
