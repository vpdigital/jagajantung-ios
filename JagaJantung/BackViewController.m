//
//  BackViewController.m
//  JagaJantung
//
//  Created by David Christian on 2/5/15.
//  Copyright (c) 2015 CodeSpade. All rights reserved.
//

#import "BackViewController.h"

@interface BackViewController ()

@end

@implementation BackViewController{
    UIButton *back;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    back = [[UIButton alloc] initWithFrame:CGRectMake(16, 15, 100, 25)];
    [back setBackgroundImage:[UIImage imageNamed:@"back"] forState:UIControlStateNormal];
    
    [back sizeToFit];
    [self.view addSubview:back];
    
    [back addTarget:self action:@selector(back) forControlEvents:UIControlEventTouchUpInside];
    
}

-(void)back{
    [back removeFromSuperview];
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
