//
//  HistoryViewController.m
//  JagaJantung
//
//  Created by David Christian on 1/18/15.
//  Copyright (c) 2015 CodeSpade. All rights reserved.
//

#import "HistoryViewController.h"
#import "HistoryCell.h"
#import "History.h"

@interface HistoryViewController ()

@end

@implementation HistoryViewController{
    NSMutableArray *histories;
    NSIndexPath *deleteIndex;
    NSMutableString *tempString;
    
}

- (void)viewDidLoad {
    [super viewDidLoad];
    tempString = [[NSMutableString alloc] initWithString:@""];
    
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entityDescription = [NSEntityDescription entityForName:@"History" inManagedObjectContext:self.managedObjectContext];
    
    NSSortDescriptor *sortDescriptor1 = [NSSortDescriptor sortDescriptorWithKey:@"createDate" ascending:NO];
    [fetchRequest setSortDescriptors:@[sortDescriptor1]];
    
    [fetchRequest setEntity:entityDescription];
    
    NSError *error = nil;
    histories = [NSMutableArray arrayWithArray:[self.managedObjectContext executeFetchRequest:fetchRequest error:&error]];
    
    
    _barGraph.autoresizingMask=(UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth);
    [self.view addSubview:_barGraph];
    NSMutableArray * dataArray= [NSMutableArray array];
    
    int i = 0;
    for (History *history in histories) {
        if(i<5){
            [tempString appendString:[NSString stringWithFormat:@"%@ bpm: %@ sys: %@ dia: %@ \r", history.createDate, history.bpm, history.sys, history.dia]];
        }
        NQData * data=[NQData dataWithDate:history.createDate andNumber:history.bpm];
        [dataArray addObject:data];
    }
    
    _barGraph.dataSource=dataArray;
    
}

#pragma mark - UIScrollViewDelegate

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    HistoryCell *cell = [self.tableView dequeueReusableCellWithIdentifier:@"HistoryCell" forIndexPath:indexPath];
    
    if(indexPath.row == 0){
        [cell.dateLabel setTextColor:[UIColor whiteColor]];
        [cell.bpmLabel setTextColor:[UIColor whiteColor]];
        [cell.kondisiLabel setTextColor:[UIColor whiteColor]];
        [cell.sysLabel setTextColor:[UIColor whiteColor]];
        [cell.diaLabel setTextColor:[UIColor whiteColor]];
        
        cell.dateLabel.text = @"Tanggal dan Jam";
        cell.bpmLabel.text = @"Denyut Jantung";
        cell.kondisiLabel.text = @"Kondisi";
        cell.sysLabel.text = @"Tekanan darah Sistolik";
        cell.diaLabel.text = @"Tekanan darah Diastolik";
        [cell setBackgroundColor:[JJColor themeColor]];
    }else{
        History *history = [histories objectAtIndex:indexPath.row - 1];
        [cell.dateLabel setTextColor:[UIColor blackColor]];
        [cell.bpmLabel setTextColor:[UIColor blackColor]];
        [cell.kondisiLabel setTextColor:[UIColor blackColor]];
        [cell.sysLabel setTextColor:[UIColor blackColor]];
        [cell.diaLabel setTextColor:[UIColor blackColor]];
        
        cell.dateLabel.text = [self formatShortDate:history.createDate];
        cell.bpmLabel.text = [history.bpm stringValue];
        cell.kondisiLabel.text = history.stat;
        cell.sysLabel.text = [history.sys stringValue];
        cell.diaLabel.text = [history.dia stringValue];;
        if(indexPath.row % 2 == 0){
        [cell setBackgroundColor:[UIColor colorWithRed:234.0/255.0 green:239.0/255.0 blue:247.0/255.0 alpha:1]];
        }else{
            [cell setBackgroundColor:[UIColor whiteColor]];
        }
    }
//
//
//    // optionally specify a width that each set of utility buttons will share
//    [cell setLeftUtilityButtons:[self leftButtons] WithButtonWidth:75.0f];
//    cell.delegate = self;
    
    return cell;
    
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if(indexPath.row == 0){
        return 40;
    }else{
        return 30;
    }
}

- (NSArray *)leftButtons
{
    NSMutableArray *leftUtilityButtons = [NSMutableArray new];
    
    [leftUtilityButtons sw_addUtilityButtonWithColor:
     [JJColor themeColor] icon:[HistoryViewController imageWithImage:[UIImage imageNamed:@"trash.png"]scaledToSize:CGSizeMake(25,25) ]];
    
    return leftUtilityButtons;
}

#pragma mark UITableViewDataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return histories.count+1;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    NSLog(@"cell selected at index path %ld:%ld", (long)indexPath.section, (long)indexPath.row);
    NSLog(@"selected cell index path is %@", [self.tableView indexPathForSelectedRow]);
    
    if (!tableView.isEditing) {
        [tableView deselectRowAtIndexPath:indexPath animated:YES];
    }
}

//- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
//    return _sections[section];
//}


// Set row height on an individual basis

//- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
//    return [self rowHeightForIndexPath:indexPath];
//}
//
//- (CGFloat)rowHeightForIndexPath:(NSIndexPath *)indexPath {
//    return ([indexPath row] * 10) + 60;
//}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    // Set background color of cell here if you don't want default white
}

- (IBAction)doShare:(id)sender {
    UIAlertView *shareAlert = [[UIAlertView alloc] initWithTitle:@"Share via" message:@"" delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"Email", @"SMS", nil];
    [shareAlert show];
}

#pragma mark - UIAlertView
-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    if(buttonIndex==1){ //via email
        MFMailComposeViewController *mc = [[MFMailComposeViewController alloc] init];
        if([MFMailComposeViewController canSendMail]){
            mc.mailComposeDelegate = self;
            [mc setSubject:@"Jaga Jantung Result History"];
            [mc setMessageBody:tempString isHTML:NO];
            [self presentViewController:mc animated:YES completion:NULL];
        }else{
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Can not open mail" message:@"Please check your mail settings" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [alert show];
        }
    }else if(buttonIndex == 2){ //via sms
        MFMessageComposeViewController *controller = [[MFMessageComposeViewController alloc] init];
        if([MFMessageComposeViewController canSendText])
        {
            controller.body = tempString;
            controller.recipients = [NSArray arrayWithObjects:nil];
            controller.messageComposeDelegate = self;
            [self presentViewController:controller animated:YES completion:nil];
        }
    }
}

#pragma mark - SMS
- (void)messageComposeViewController:(MFMessageComposeViewController *)controller didFinishWithResult:(MessageComposeResult)result
{
    switch (result) {
        case MessageComposeResultCancelled:{
            NSLog(@"Cancelled");
        }
            break;
        case MessageComposeResultFailed:{
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Unknown Error"
                                                           delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
            [alert show];
        }
            break;
        case MessageComposeResultSent:
            
            break;
        default:
            break;
    }
    
    [self dismissViewControllerAnimated:YES completion:nil];
}
#pragma mark - Email
- (void) mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error
{
    switch (result)
    {
        case MFMailComposeResultCancelled:
            NSLog(@"Mail cancelled");
            break;
        case MFMailComposeResultSaved:
            NSLog(@"Mail saved");
            break;
        case MFMailComposeResultSent:
            NSLog(@"Mail sent");
            break;
        case MFMailComposeResultFailed:
            NSLog(@"Mail sent failure: %@", [error localizedDescription]);
            break;
        default:
            break;
    }
    
    // Close the Mail Interface
    [self dismissViewControllerAnimated:YES completion:NULL];
}


//#pragma mark - SWTableViewDelegate
//
//- (void)swipeableTableViewCell:(SWTableViewCell *)cell scrollingToState:(SWCellState)state
//{
//    switch (state) {
//        case 0:
//            NSLog(@"utility buttons closed");
//            break;
//        case 1:
//            NSLog(@"left utility buttons open");
//            break;
//        case 2:
//            NSLog(@"right utility buttons open");
//            break;
//        default:
//            break;
//    }
//}
//
//- (void)swipeableTableViewCell:(SWTableViewCell *)cell didTriggerLeftUtilityButtonWithIndex:(NSInteger)index
//{
//    switch (index) {
//        case 0:{
//            NSLog(@"Delete");
//            deleteIndex = [_tableView indexPathForCell:cell];
//            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Hapus Data" message:@"Apakah anda ingin menghapus data ini?" delegate:self cancelButtonTitle:@"Batal" otherButtonTitles:@"Ya", nil];
//            [alert show];
//        }
//            break;
//        case 1:
//            NSLog(@"left button 1 was pressed");
//            break;
//        case 2:
//            NSLog(@"left button 2 was pressed");
//            break;
//        case 3:
//            NSLog(@"left btton 3 was pressed");
//        default:
//            break;
//    }
//}
//
//- (void)swipeableTableViewCell:(SWTableViewCell *)cell didTriggerRightUtilityButtonWithIndex:(NSInteger)index
//{
//    switch (index) {
//        case 0:
//        {
//            NSLog(@"More button was pressed");
//            UIAlertView *alertTest = [[UIAlertView alloc] initWithTitle:@"Hello" message:@"More more more" delegate:nil cancelButtonTitle:@"cancel" otherButtonTitles: nil];
//            [alertTest show];
//            
//            [cell hideUtilityButtonsAnimated:YES];
//            break;
//        }
//        case 1:
//        {
//            // Delete button was pressed
//            NSIndexPath *cellIndexPath = [self.tableView indexPathForCell:cell];
//            
////            [_testArray[cellIndexPath.section] removeObjectAtIndex:cellIndexPath.row];
//            [self.tableView deleteRowsAtIndexPaths:@[cellIndexPath] withRowAnimation:UITableViewRowAnimationLeft];
//            break;
//        }
//        default:
//            break;
//    }
//}
//
//- (BOOL)swipeableTableViewCellShouldHideUtilityButtonsOnSwipe:(SWTableViewCell *)cell
//{
//    // allow just one cell's utility button to be open at once
//    return YES;
//}
//
//- (BOOL)swipeableTableViewCell:(SWTableViewCell *)cell canSwipeToState:(SWCellState)state
//{
//    switch (state) {
//        case 1:
//            // set to NO to disable all left utility buttons appearing
//            return YES;
//            break;
//        case 2:
//            // set to NO to disable all right utility buttons appearing
//            return YES;
//            break;
//        default:
//            break;
//    }
//    
//    return YES;
//}
//
//#pragma UIAlertView
//-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
//    if(buttonIndex == 1){
//        NSLog(@"Hapus");
//        History *history = [histories objectAtIndex:deleteIndex.row];
//        [histories removeObject:history];
//        
//        [self.managedObjectContext deleteObject:history];
//        
//        NSError *deleteError = nil;
//        
//        if (![history.managedObjectContext save:&deleteError]) {
//            NSLog(@"Unable to save managed object context.");
//            NSLog(@"%@, %@", deleteError, deleteError.localizedDescription);
//        }
//        
//        [_tableView reloadData];
//        
////        [self setupGraph];
//    }
//}
//
+ (UIImage *)imageWithImage:(UIImage *)image scaledToSize:(CGSize)newSize {
    //UIGraphicsBeginImageContext(newSize);
    // In next line, pass 0.0 to use the current device's pixel scaling factor (and thus account for Retina resolution).
    // Pass 1.0 to force exact pixel size.
    UIGraphicsBeginImageContextWithOptions(newSize, NO, 0.0);
    [image drawInRect:CGRectMake(0, 0, newSize.width, newSize.height)];
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return newImage;
}

- (NSString*)formatShortDate:(NSDate*)date{
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"dd/MM"];
    
    return [dateFormat stringFromDate:date];
}
//
//- (NSString*)formatDate:(NSDate*)date{
//    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
//    [dateFormat setDateFormat:@"dd/MM/yy"];
//    
//    return [dateFormat stringFromDate:date];
//}
//
//- (NSString*)formatTime:(NSDate*)date{
//    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
//    [dateFormat setDateFormat:@"HH:mm"];
//    
//    return [dateFormat stringFromDate:date];
//}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
