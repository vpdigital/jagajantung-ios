//
//  BaseNavigationViewController.m
//  JagaJantung
//
//  Created by David Christian on 1/7/15.
//  Copyright (c) 2015 CodeSpade. All rights reserved.
//

#import "BaseNavigationViewController.h"
#import "HeartRateViewController.h"
#import "TermsAndConditionsViewController.h"

@interface BaseNavigationViewController ()

@end

@implementation BaseNavigationViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self.navigationBar setBackgroundImage:[UIImage new]
                             forBarMetrics:UIBarMetricsDefault];
    self.navigationBar.shadowImage = [UIImage new];
    self.navigationBar.translucent = YES;
    self.navigationItem.hidesBackButton = YES;
    
//    UIImage *header = [UIImage imageNamed:@"title"];
//    UIImageView *imageView = [[UIImageView alloc] initWithImage:header];
//    CGSize barSize = self.navigationController.navigationBar.frame.size;
//    imageView.frame = CGRectMake(70, 5, barSize.width - 140, barSize.height);
//    imageView.contentMode = UIViewContentModeCenter;
//    imageView.contentMode = UIViewContentModeScaleAspectFit;
//    [self.view addSubview:imageView];
//    
//    CGSize barSize = self.navigationController.navigationBar.frame.size;
//    UIImageView *imgView = [[UIImageView alloc] initWithFrame:CGRectMake(barSize.width - 60, 15, 25, 25)];
//    [imgView setImage:[UIImage imageNamed:@"question"]];
//    imgView.contentMode = UIViewContentModeCenter;
//    imgView.contentMode = UIViewContentModeScaleAspectFit;
//    
//    _tncButton = [[UIBarButtonItem alloc] initWithCustomView:imgView];
//    [self.navigationItem setRightBarButtonItem:_tncButton];
//    [_tncButton setAction:@selector(toTnC)];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
