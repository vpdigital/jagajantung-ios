//
//  TipsCell.h
//  JagaJantung
//
//  Created by David Christian on 2/7/15.
//  Copyright (c) 2015 CodeSpade. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TipsCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *title;
@property (weak, nonatomic) IBOutlet UILabel *desc;
@property (weak, nonatomic) IBOutlet UIButton *button;

@end
