//
//  JagaJantungViewController.m
//  JagaJantung
//
//  Created by David Christian on 1/19/15.
//  Copyright (c) 2015 CodeSpade. All rights reserved.
//

#import "JagaJantungViewController.h"
#import "MenuViewController.h"
#import "BaseNavigationViewController.h"

@interface JagaJantungViewController ()

@end

@implementation JagaJantungViewController

- (void)awakeFromNib
{
    self.liveBlur = NO;
    self.blurTintColor = [UIColor colorWithWhite:0 alpha:0.1f];
    self.blurRadius = 3.0f;
    
    BaseNavigationViewController *content= [self.storyboard instantiateViewControllerWithIdentifier:@"CONTENT_CONTROLLER"];
    
    MenuViewController *menuVC = [self.storyboard instantiateViewControllerWithIdentifier:@"MENU_CONTROLLER"];
    menuVC.navigationVC = content;
    
    self.contentViewController = content;
    self.menuViewController = menuVC;
    
    self.liveBlurBackgroundStyle = REFrostedViewControllerLiveBackgroundStyleDark;
    
}

@end
