//
//  BackViewController.h
//  JagaJantung
//
//  Created by David Christian on 2/5/15.
//  Copyright (c) 2015 CodeSpade. All rights reserved.
//

#import "BaseWithNavigationViewController.h"

@interface BackViewController : BaseWithNavigationViewController
@property (strong, nonatomic)BaseWithNavigationViewController* previousVC;
@property (strong, nonatomic)UIButton *backButton;
@end
