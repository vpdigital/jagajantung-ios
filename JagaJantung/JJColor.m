//
//  JJColor.m
//  JagaJantung
//
//  Created by David Christian on 1/6/15.
//  Copyright (c) 2015 CodeSpade. All rights reserved.
//

#import "JJColor.h"

@implementation JJColor
+(UIColor *)themeColor{
    return [UIColor colorWithRed:217.0/255.0 green:23.0/255.0 blue:95.0/255.0 alpha:1];
}

+(UIColor *)shadow{
    return [UIColor colorWithRed:171.0/255.0 green:22.0/255.0 blue:74.0/255.0 alpha:1];
}

+(UIColor *)progressColor{
    return [UIColor colorWithRed:69.0/255.0 green:216.0/255.0 blue:252.0/255.0 alpha:1];
}
@end
