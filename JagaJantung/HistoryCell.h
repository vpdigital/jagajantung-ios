//
//  HistoryCell.h
//  JagaJantung
//
//  Created by David Christian on 1/18/15.
//  Copyright (c) 2015 CodeSpade. All rights reserved.
//

#import "SWTableViewCell.h"
#import "RoundedLabel.h"

@interface HistoryCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *dateLabel;
@property (weak, nonatomic) IBOutlet UILabel *bpmLabel;
@property (weak, nonatomic) IBOutlet UILabel *kondisiLabel;
@property (weak, nonatomic) IBOutlet UILabel *sysLabel;
@property (weak, nonatomic) IBOutlet UILabel *diaLabel;

@end
