//
//  BaseTabBarViewController.h
//  JagaJantung
//
//  Created by David Christian on 2/4/15.
//  Copyright (c) 2015 CodeSpade. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BaseTabBarViewController : UITabBarController<UITabBarControllerDelegate>

@end
