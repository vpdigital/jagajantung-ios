//
//  MenuViewController.h
//  JagaJantung
//
//  Created by David Christian on 1/19/15.
//  Copyright (c) 2015 CodeSpade. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <REFrostedViewController/REFrostedViewController.h>
#import "BaseNavigationViewController.h"

@interface MenuViewController : UIViewController <UITableViewDelegate, UITableViewDataSource>
@property (strong, nonatomic) BaseNavigationViewController *navigationVC;

@end
