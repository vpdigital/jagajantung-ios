//
//  HistoryViewController.h
//  JagaJantung
//
//  Created by David Christian on 1/18/15.
//  Copyright (c) 2015 CodeSpade. All rights reserved.
//

#import "BaseWithNavigationViewController.h"
#import <SWTableViewCell/SWTableViewCell.h>
#import <SHLineGraphView/SHLineGraphView.h>
#import <SHLineGraphView/SHPlot.h>
#import "NQBarGraph.h"
#import "NQData.h"
#import <MessageUI/MessageUI.h>

@interface HistoryViewController : BaseWithNavigationViewController<UITableViewDataSource, UITableViewDelegate, SWTableViewCellDelegate, UIAlertViewDelegate, MFMailComposeViewControllerDelegate, MFMailComposeViewControllerDelegate>
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet NQBarGraph *barGraph;

@end
