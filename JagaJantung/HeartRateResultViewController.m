//
//  HeartRateResultViewController.m
//  JagaJantung
//
//  Created by David Christian on 1/7/15.
//  Copyright (c) 2015 CodeSpade. All rights reserved.
//

#import "HeartRateResultViewController.h"
#import "HeartRateViewController.h"
#import "ResultViewController.h"

@interface HeartRateResultViewController ()
@property (strong, nonatomic) YMPulseRateMater *pulseRateMeter;

@end

@implementation HeartRateResultViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    _resultLabel.hidden = YES;
    _mengukurLabel.alpha = 0;
    _progressView.progress = 0;
    [UIView animateWithDuration:1 delay:0.5 options:UIViewAnimationOptionRepeat | UIViewAnimationOptionAutoreverse animations:^{
        _mengukurLabel.alpha = 1;
    } completion:nil];
    
    // Do any additional setup after loading the view.
    self.pulseRateMeter = [YMPulseRateMater new];
    [self.pulseRateMeter setDelegate:self];
    [_progressView setProgress:_bpmResult/200.0 animated:YES];
    self.tabBarController.tabBar.userInteractionEnabled = NO;
    
    [self.pulseRateMeter start];
    
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)berhenti:(id)sender {
    [self.pulseRateMeter stop];
    
    self.tabBarController.tabBar.userInteractionEnabled = YES;
    
    ResultViewController *destination = [self.storyboard instantiateViewControllerWithIdentifier:@"HeartRate"];
    [self.navigationController pushViewController:destination animated:YES];
}

#pragma PulseRateMeter
- (void)pulseRateMeter:(id)sender completeWithPulseRate:(float)pulseRate{
    //    [DejalBezelActivityView removeView];
    self.tabBarController.tabBar.userInteractionEnabled = YES;
    
    HeartRateResultViewController *heartRateResultVC = [self.storyboard instantiateViewControllerWithIdentifier:@"Result"];
    heartRateResultVC.bpmResult = pulseRate;
    [self.navigationController pushViewController:heartRateResultVC animated:NO];
}

-(void)updateProgress:(float)progress{
//            NSLog([NSString stringWithFormat:@"Main thread = %d", [NSThread isMainThread]]);
//            NSLog([NSString stringWithFormat:@"%f", progress]);
            //    [[NSOperationQueue mainQueue] addOperationWithBlock:^ {
            
            //Your code goes in here
            [_progressView setProgress:progress animated:YES];
            
            //    }];
    

}

-(void)updatePulse:(float)pulse{
    [_resultLabel setText:[NSString stringWithFormat:@"%0.f kali per menit", pulse]];
    _resultLabel.hidden = NO;
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
