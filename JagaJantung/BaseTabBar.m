//
//  BaseTabBar.m
//  JagaJantung
//
//  Created by David Christian on 2/4/15.
//  Copyright (c) 2015 CodeSpade. All rights reserved.
//

#import "BaseTabBar.h"

@implementation BaseTabBar

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

-(CGSize)sizeThatFits:(CGSize)size{
    CGSize sizeThatFits = [super sizeThatFits:size];
    sizeThatFits.height = 60;
    
    return sizeThatFits;
}

@end
