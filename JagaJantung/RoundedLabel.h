//
//  StatusLabel.h
//  JagaJantung
//
//  Created by David Christian on 1/10/15.
//  Copyright (c) 2015 CodeSpade. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RoundedLabel : UILabel

@end
