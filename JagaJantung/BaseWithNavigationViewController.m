//
//  BaseWithNavigationViewController.m
//  JagaJantung
//
//  Created by David Christian on 1/9/15.
//  Copyright (c) 2015 CodeSpade. All rights reserved.
//

#import "BaseWithNavigationViewController.h"
#import "TermsAndConditionsViewController.h"

@interface BaseWithNavigationViewController ()

@end

@implementation BaseWithNavigationViewController{
    UIBarButtonItem *forward;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
//    [self.navigationController.navigationBar setBackgroundImage:[UIImage new]
//                                                  forBarMetrics:UIBarMetricsDefault];
//    self.navigationController.navigationBar.shadowImage = [UIImage new];
//    self.navigationController.navigationBar.translucent = YES;
//    self.navigationController.view.backgroundColor = [UIColor clearColor];
//    self.navigationController.navigationBar.backgroundColor = [UIColor clearColor];
    
    self.navigationController.navigationBarHidden = YES;
    
    self.navigationController.navigationBar.userInteractionEnabled = NO;
    
//    self.navigationItem.hidesBackButton = YES;
    
    
    UIImage *header = [UIImage imageNamed:@"title"];
    UIImageView *imageView = [[UIImageView alloc] initWithImage:header];
    CGSize barSize = self.navigationController.navigationBar.frame.size;
    imageView.frame = CGRectMake(70, 5, barSize.width - 140, barSize.height);
    imageView.contentMode = UIViewContentModeCenter;
    imageView.contentMode = UIViewContentModeScaleAspectFit;
    [self.view addSubview:imageView];
    
    _tncButton = [[UIButton alloc] initWithFrame:CGRectMake(barSize.width - 60, 15, 25, 25)];
    [_tncButton setBackgroundImage:[UIImage imageNamed:@"question"] forState:UIControlStateNormal];
    [self.view addSubview:_tncButton];
    
    [_tncButton addTarget:self action:@selector(toTnC) forControlEvents:UIControlEventTouchUpInside];
}

-(void)toTnC{
    TermsAndConditionsViewController *destination = (TermsAndConditionsViewController*)[self.storyboard instantiateViewControllerWithIdentifier:@"TnC"];
    //    destination.previousVC = self;
    //    [self addChildViewController:destination];
    //    [destination didMoveToParentViewController:self];
    [self.navigationController pushViewController:destination animated:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)hideRButton{
    self.navigationItem.rightBarButtonItem = nil;
}

-(void)showRButton{
    self.navigationItem.rightBarButtonItem = forward;
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
