//
//  BaseTabBarViewController.m
//  JagaJantung
//
//  Created by David Christian on 2/4/15.
//  Copyright (c) 2015 CodeSpade. All rights reserved.
//

#import "BaseTabBarViewController.h"

@interface BaseTabBarViewController ()

@end

@implementation BaseTabBarViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [[self tabBar] setSelectedImageTintColor:[UIColor colorWithRed:210.0/255.0 green:34.0/255.0 blue:77.0/255.0 alpha:1]];
    [self setDelegate:self];
    [self setSelectedIndex:1];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)tabBarController:(UITabBarController *)tabBarController didSelectViewController:(UIViewController *)viewController{
    int tabitem = tabBarController.selectedIndex;
    UINavigationController *vc = [tabBarController.viewControllers objectAtIndex:tabitem];
    if(tabBarController.selectedIndex == 0){
        [vc pushViewController:[self.storyboard instantiateViewControllerWithIdentifier:@"Tips"] animated:NO];
    }else if(tabBarController.selectedIndex == 1){
        [vc pushViewController:[self.storyboard instantiateViewControllerWithIdentifier:@"HeartRate"] animated:NO];
    }else if(tabBarController.selectedIndex == 2){
        [vc pushViewController:[self.storyboard instantiateViewControllerWithIdentifier:@"History"] animated:NO];
    }
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
