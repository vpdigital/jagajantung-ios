//
//  BaseViewController.h
//  Jaga Jantung
//
//  Created by David Christian on 12/27/14.
//  Copyright (c) 2014 CodeSpade. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "JJColor.h"
#import <REFrostedViewController/REFrostedViewController.h>
#import "AppDelegate.h"

@interface BaseViewController : UIViewController
@property (strong, nonatomic) UIWindow *window;
@property (strong, nonatomic) UIViewController *viewController;

@property (readonly, nonatomic) NSManagedObjectContext *managedObjectContext;

@end
