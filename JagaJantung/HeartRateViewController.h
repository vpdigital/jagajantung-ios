//
//  HeartRateViewController.h
//  JagaJantung
//
//  Created by David Christian on 1/7/15.
//  Copyright (c) 2015 CodeSpade. All rights reserved.
//

#import "BaseWithNavigationViewController.h"
#import "DACircularProgressView.h"
#import "YMPulseRateMater.h"

@interface HeartRateViewController : BaseWithNavigationViewController<YMPulseRateMeterDelegate>
@property (weak, nonatomic) IBOutlet DACircularProgressView *progressView;

@property (weak, nonatomic) IBOutlet UIButton *mulaiBTN;

@end
