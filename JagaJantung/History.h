//
//  History.h
//  JagaJantung
//
//  Created by David Christian on 2/8/15.
//  Copyright (c) 2015 CodeSpade. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface History : NSManagedObject

@property (nonatomic, retain) NSNumber * bpm;
@property (nonatomic, retain) NSDate * createDate;
@property (nonatomic, retain) NSNumber * dia;
@property (nonatomic, retain) NSNumber * sys;
@property (nonatomic, retain) NSString * stat;

@end
