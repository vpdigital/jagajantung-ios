//
//  ResultViewController.h
//  JagaJantung
//
//  Created by David Christian on 1/10/15.
//  Copyright (c) 2015 CodeSpade. All rights reserved.
//

#import "BaseWithNavigationViewController.h"
#import "RoundedLabel.h"
#import <MessageUI/MessageUI.h>

@interface ResultViewController : BaseWithNavigationViewController<MFMailComposeViewControllerDelegate>
@property (weak, nonatomic) IBOutlet UIControl *normalControl;
@property (weak, nonatomic) IBOutlet UIControl *banyakPikiranControl;
@property (weak, nonatomic) IBOutlet UIControl *santaiControl;
@property (weak, nonatomic) IBOutlet UILabel *normalLabel;
@property (weak, nonatomic) IBOutlet UILabel *banyakPikiranLabel;
@property (weak, nonatomic) IBOutlet UILabel *santaiLabel;

@property (weak, nonatomic) IBOutlet UILabel *resultLabel;

@property (weak, nonatomic) IBOutlet UITextView *descriptionView;
@property (weak, nonatomic) IBOutlet UIButton *more;
@property (weak, nonatomic) IBOutlet UIButton *simpan;

@property NSInteger bpmResult;

@end
