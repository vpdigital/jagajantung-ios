//
//  BaseNavigationViewController.h
//  JagaJantung
//
//  Created by David Christian on 1/7/15.
//  Copyright (c) 2015 CodeSpade. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BaseNavigationViewController : UINavigationController
@property (strong, nonatomic)UIBarButtonItem *tncButton;

@end
