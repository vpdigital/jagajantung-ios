//
//  MenuViewController.m
//  JagaJantung
//
//  Created by David Christian on 1/19/15.
//  Copyright (c) 2015 CodeSpade. All rights reserved.
//

#import "MenuViewController.h"
#import "MenuCell.h"
#import "HeartRateViewController.h"
#import "HistoryViewController.h"
#import "TermsAndConditionsViewController.h"

#define HEART_RATE 0
#define HISTORY 1
#define EDIT_PROFILE 2
#define CLOSE 0
#define TERMS_CONDITION 1

@interface MenuViewController ()

@end

@implementation MenuViewController

- (void)viewDidLoad {
    [super viewDidLoad];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark UITableView
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 2;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if(section == 0){
        return 3;
    }else{
        return 2;
    }
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"MENU_CELL";
    MenuCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    
    if(indexPath.section == 0){
        switch (indexPath.row) {
            case HEART_RATE:
                [cell.label setText:@"Heart Rate"];
                break;
            case HISTORY:
                [cell.label setText:@"History"];
                break;
            case EDIT_PROFILE:
                [cell.label setText:@"Edit Profile"];
                break;
            default:
                break;
        }
    }else{
        switch (indexPath.row) {
            case CLOSE:
                [cell.label setText:@"Close"];
                break;
            case TERMS_CONDITION:
                [cell.label setText:@"Term and Condition"];
                break;
            default:
                break;
        }
    }
    
    return cell; 
}

-(UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section{
    UIView *viewHeader = [UIView.alloc initWithFrame:CGRectMake(0, 0, tableView.frame.size.width, 28)];
    [viewHeader setBackgroundColor:[UIColor colorWithWhite:0 alpha:1]];
    return viewHeader;
    
}


-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    if (section==1) {
        return 0;
    }else{
        return 28;
    }
}

-(void)tableView:(UITableView *)tableView didHighlightRowAtIndexPath:(NSIndexPath *)indexPath{
    UITableViewCell *cell = (UITableViewCell *)[tableView cellForRowAtIndexPath:indexPath];
    [cell setBackgroundColor:[UIColor grayColor]];  //highlight colour
}

- (void)tableView:(UITableView *)tableView didUnhighlightRowAtIndexPath:(NSIndexPath *)indexPath {
    // Reset Colour.
    UITableViewCell *cell = (UITableViewCell *)[tableView cellForRowAtIndexPath:indexPath];
    [cell setBackgroundColor:[UIColor clearColor]];  //highlight colour
    
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{

    if(indexPath.section == 0){
        if(indexPath.row == HEART_RATE){
            HeartRateViewController *heartRateVC = [self.storyboard instantiateViewControllerWithIdentifier:@"HeartRate"];
            [_navigationVC pushViewController:heartRateVC animated:YES];
        }else if(indexPath.row == HISTORY){
            HistoryViewController *historyVC = [self.storyboard instantiateViewControllerWithIdentifier:@"History"];
            [_navigationVC pushViewController:historyVC animated:YES];
        }
    }else{
        if(indexPath.row == CLOSE){
            exit(0);
        }else if(indexPath.row == TERMS_CONDITION){
            TermsAndConditionsViewController *tncVC = [self.storyboard instantiateViewControllerWithIdentifier:@"TnC"];
            [_navigationVC pushViewController:tncVC animated:YES];
        }
    }
    
    [self.frostedViewController hideMenuViewController];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
