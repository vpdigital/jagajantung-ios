//
//  MenuCell.h
//  JagaJantung
//
//  Created by David Christian on 1/19/15.
//  Copyright (c) 2015 CodeSpade. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MenuCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *label;

@end
