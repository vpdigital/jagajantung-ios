//
//  JJButton.m
//  JagaJantung
//
//  Created by David Christian on 1/6/15.
//  Copyright (c) 2015 CodeSpade. All rights reserved.
//

#import "JJButton.h"
#import "JJColor.h"

@implementation JJButton

-(id)initWithCoder:(NSCoder *)aDecoder{
    self = [super initWithCoder:aDecoder];
    
    self.layer.cornerRadius = 5;
    self.clipsToBounds = YES;
    
    self.backgroundColor = [UIColor colorWithWhite:216.0/255.0 alpha:1];
    self.layer.borderColor = [UIColor lightGrayColor].CGColor;
    self.layer.borderWidth = 1;
    self.tintColor = [JJColor themeColor];
    
    return self;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
