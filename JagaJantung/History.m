//
//  History.m
//  JagaJantung
//
//  Created by David Christian on 2/8/15.
//  Copyright (c) 2015 CodeSpade. All rights reserved.
//

#import "History.h"


@implementation History

@dynamic bpm;
@dynamic createDate;
@dynamic dia;
@dynamic sys;
@dynamic stat;

@end
