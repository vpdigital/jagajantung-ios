//
//  BloodPressureViewController.m
//  JagaJantung
//
//  Created by David Christian on 1/10/15.
//  Copyright (c) 2015 CodeSpade. All rights reserved.
//

#import "BloodPressureViewController.h"
#import "HistoryViewController.h"
#import "History.h"
#import "BaseTabBarViewController.h"

@interface BloodPressureViewController ()

@end

@implementation BloodPressureViewController{
    NSArray *pickerData;
    NSInteger sysResult;
    NSInteger diaResult;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    NSMutableArray *systolic = [[NSMutableArray alloc] init];
    NSMutableArray *diastolic = [[NSMutableArray alloc] init];
    
    _sys.layer.cornerRadius = 5;
    _sys.layer.masksToBounds = YES;
    _dia.layer.cornerRadius = 5;
    _dia.layer.masksToBounds = YES;
    
    CAGradientLayer *gradient = [CAGradientLayer layer];
    gradient.frame = CGRectMake(0, 0, 450, 60);
    gradient.colors = [NSArray arrayWithObjects:(id)[[UIColor colorWithRed:233.0/255.0 green:50.0/255.0 blue:121.0/255.0 alpha:1] CGColor], (id)[[UIColor colorWithRed:206.0/255.0 green:44.0/255.0 blue:107.0/255.0 alpha:1] CGColor], nil];
    
    [_selectionView.layer insertSublayer:gradient atIndex:0];
    
    for (int i = 80; i<=200; i++) {
        [systolic addObject:[NSString stringWithFormat:@"%d", i]];
    }
    
    for (int i = 30; i<=150; i++) {
        [diastolic addObject:[NSString stringWithFormat:@"%d", i]];
    }
    
    
    // Initialize Data
    pickerData = @[ systolic, diastolic];
    
    [_bloodPressurePicker selectRow:40 inComponent:0 animated:NO];
    [_bloodPressurePicker selectRow:50 inComponent:1 animated:NO];
    
    
    sysResult = [pickerData[0][40] integerValue];
    diaResult = [pickerData[1][50] integerValue];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)doLewati:(id)sender {
    NSEntityDescription *entityDescription = [NSEntityDescription entityForName:@"History" inManagedObjectContext:self.managedObjectContext];
    
    History *history = [[History alloc] initWithEntity:entityDescription insertIntoManagedObjectContext:self.managedObjectContext];
    history.sys = [NSNumber numberWithInteger:0];
    history.dia = [NSNumber numberWithInteger:0];
    history.bpm = [NSNumber numberWithInteger:_bpmResult];
    history.stat = _status;
    history.createDate = [NSDate date];
    
    NSError *error;
    
    
    if (![history.managedObjectContext save:&error]) {
        NSLog(@"Unable to save managed object context.");
        NSLog(@"%@, %@", error, error.localizedDescription);
    }else{
        BaseTabBarViewController *tabVC = (BaseTabBarViewController*)self.tabBarController;
        [tabVC setSelectedIndex:2];
        UINavigationController *vc = [tabVC.viewControllers objectAtIndex:2];
        [vc pushViewController:[self.storyboard instantiateViewControllerWithIdentifier:@"History"] animated:NO];
    }
}
- (IBAction)doSimpan:(id)sender {
    NSEntityDescription *entityDescription = [NSEntityDescription entityForName:@"History" inManagedObjectContext:self.managedObjectContext];
    
    History *history = [[History alloc] initWithEntity:entityDescription insertIntoManagedObjectContext:self.managedObjectContext];
    history.sys = [NSNumber numberWithInteger:sysResult];
    history.dia = [NSNumber numberWithInteger:diaResult];
    history.bpm = [NSNumber numberWithInteger:_bpmResult];
    history.stat = _status;
    history.createDate = [NSDate date];
    
    NSError *error;
    
    
    if (![history.managedObjectContext save:&error]) {
        NSLog(@"Unable to save managed object context.");
        NSLog(@"%@, %@", error, error.localizedDescription);
    }else{
        BaseTabBarViewController *tabVC = (BaseTabBarViewController*)self.tabBarController;
        [tabVC setSelectedIndex:2];
        UINavigationController *vc = [tabVC.viewControllers objectAtIndex:2];
        [vc pushViewController:[self.storyboard instantiateViewControllerWithIdentifier:@"History"] animated:NO];
    }
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark UIPickerView
// The number of columns of data
- (int)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    return 2;
}

// The number of rows of data
- (int)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    return [(NSArray*)pickerData[component] count];
    
}

// The data to return for the row and component (column) that's being passed in

- (NSAttributedString *)pickerView:(UIPickerView *)pickerView attributedTitleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    NSString *title = pickerData[component][row];
    UIFont *font = [UIFont fontWithName:@"OpenSans" size:2];
    NSAttributedString *attString = [[NSAttributedString alloc] initWithString:title attributes:@{NSForegroundColorAttributeName:[UIColor blackColor], NSFontAttributeName:font}];
    return attString;
    
}

-(void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component{
    if(component == 0){
        sysResult = [pickerData[component][row] integerValue];
    }else{
        diaResult = [pickerData[component][row] integerValue];
    }
}

-(CGFloat)pickerView:(UIPickerView *)pickerView rowHeightForComponent:(NSInteger)component{
    return 60;
}

//-(UIView *)pickerView:(UIPickerView *)pickerView viewForRow:(NSInteger)row forComponent:(NSInteger)component reusingView:(UIView *)view{
//    UIView *v = [[UIView alloc] init];
//    return v;
//}

@end
