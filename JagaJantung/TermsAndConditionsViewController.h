//
//  TermsAndConditionsViewController.h
//  JagaJantung
//
//  Created by David Christian on 1/16/15.
//  Copyright (c) 2015 CodeSpade. All rights reserved.
//

#import "BackViewController.h"

@interface TermsAndConditionsViewController : BackViewController
@property (weak, nonatomic) IBOutlet UITextView *termsTextView;

@end
