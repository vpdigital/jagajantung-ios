//
//  StatusLabel.m
//  JagaJantung
//
//  Created by David Christian on 1/10/15.
//  Copyright (c) 2015 CodeSpade. All rights reserved.
//

#import "RoundedLabel.h"

@implementation RoundedLabel

-(id)initWithCoder:(NSCoder *)aDecoder{
    self = [super initWithCoder:aDecoder];
    
    self.layer.cornerRadius = 5;
    self.layer.masksToBounds = YES;
    
    return self;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
